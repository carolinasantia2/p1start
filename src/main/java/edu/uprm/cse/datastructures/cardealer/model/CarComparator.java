package edu.uprm.cse.datastructures.cardealer.model;

import java.util.Comparator;

/*Compares two cars based on a string formed with brand, model and options. 
 For example, ToyotaRav4LE goes before ToyotaRav4SE. And Honda cars go before Toyota cars.
 */

public class CarComparator implements Comparator<Car> {

	@Override
	public int compare(Car o1, Car o2) {
		String s1 = o1.getCarBrand() + "" + o1.getCarModel() + "" + o1.getCarModelOption();
		String s2 = o2.getCarBrand() + "" + o2.getCarModel() + "" + o2.getCarModelOption();

		return s1.compareTo(s2);
	}
	
	
}
