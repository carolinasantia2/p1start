package edu.uprm.cse.datastructures.cardealer.util;

import java.util.Comparator;
import java.util.Iterator;
import java.util.NoSuchElementException;

/**
 * Circular Sorted Doubly Linked List class that implements the SortedList interface.
 * Add method sorts values based on a comparator given in the CSDLList constructor.
 * Dummy header that connects the first and last none dummy nodes to itself unless the list is empty, then the header should point to itself.
 * 
 * @author Carolina Santiago
 *
 * @param <E>
 */


public class CircularSortedDoublyLinkedList<E> implements SortedList<E> {

	private int size;
	private Node<E> header;
	private Comparator<E> comp;

	/**
	 * Initializes list, sets header pointing to header on both sides, and size to 0.
	 */
	public CircularSortedDoublyLinkedList() {
		this.size = 0;
		this.header = new Node<E>(null, header, header);
	}

	/**
	 * Initializes list using the previous constructor, but adds a comparator parameter.
	 * 
	 * @param comparator - used to sort the list.
	 */
	public CircularSortedDoublyLinkedList(Comparator<E> comp) {
		this();
		this.comp = comp;
	}

	/**
	 * Iterator for the CircularSortedDoublyLinkedList.
	 */
	@Override
	public Iterator<E> iterator() {

		Iterator<E> it = new Iterator<E>() {

			private int current = 0;

			@Override
			public boolean hasNext() {
				return current < size;
			}

			@Override
			public E next() {
				if(!hasNext())
					throw new NoSuchElementException("The node does not have a next");
				return get(current++);
			}

			@Override
			public void remove() {
				throw new UnsupportedOperationException("Not being used");
			}
		};
		return it;

	}

	/**
	 * Adds an object to the list in ascending order according to the comparator's integer return. 
	 * 
	 * @param obj - cannot be null and must be a generic type.
	 * 
	 * @return boolean - if the object is added returns true, false if it is found that the object is null.
	 */
	@Override
	public boolean add(E obj) {
		if(obj == null) {
			return false;
		}
		if(this.isEmpty()) {
			Node<E> newNode = new Node<E>(obj, this.header, this.header);
			this.header.setNext(newNode);
			this.header.setPrev(newNode);
			this.size++;
			return true;
		}
		else {
			Node<E> temp = this.header.getNext();
			while(temp != header){
				if(this.comp.compare(obj,temp.getElement()) <=0) {
					Node<E> newNode = new Node<E>(obj, temp, temp.getPrev());
					temp.getPrev().setNext(newNode);
					temp.setPrev(newNode);
					this.size++;
					return true;
				}
				temp = temp.getNext();
			}
			Node<E> newNode = new Node<E>(obj, this.header, this.header.getPrev());
			this.header.getPrev().setNext(newNode);
			this.header.setPrev(newNode);
			this.size++;
			return true;
		}
	}

	/**
	 * Returns the size of the list. 
	 * @return size - size of the list
	 */
	@Override
	public int size() {
		return size;
	}

	/**
	 * If list is not empty, and object is in list, removes the object.
	 * 
	 * @param obj - cannot be null and must be a generic type.
	 * 
	 * @return boolean - if the object is removed returns true, false otherwise.
	 */
	@Override
	public boolean remove(E obj) {
		if(this.isEmpty() || obj == null || !this.contains(obj)) {
			return false;
		}
		Node<E> temp = header.getNext();
		while(temp != header) {
			if(temp.getElement().equals(obj)) {
				temp.getPrev().setNext(temp.getNext());
				temp.getNext().setPrev(temp.getPrev());
				temp.cleanLinks();
				this.size--;
				return true;
			}
			temp = temp.getNext();
		}
		return false;
	}

	/**
	 * If list is not empty, and index is valid, removes object in the index given.
	 * 
	 * @param index - valid from 0 to size-1.
	 * 
	 * @return boolean - if the object is removed returns true, false otherwise.
	 */
	@Override
	public boolean remove(int index) throws IndexOutOfBoundsException {
		if(this.isEmpty() || index < 0 || index >= this.size) {
			throw new IndexOutOfBoundsException();
		}
		Node<E> temp = header.getNext();
		for(int i = 0; i < index; i++) {
			temp = temp.getNext();
		}
		temp.getPrev().setNext(temp.getNext());
		temp.getNext().setPrev(temp.getPrev());
		temp.cleanLinks();
		this.size--;
		return true;
	}

	/**
	 * While remove(obj) is not false, removes all objects that are equal to the given object.
	 * 
	 * @param obj - cannot be null.
	 * 
	 * @return count - count of objects that were removed.
	 */
	@Override
	public int removeAll(E obj) {
		int count = 0;
		while(this.remove(obj)) {
			count++;
		}
		return count;
	}

	/**
	 * If list is not empty, returns the first element.
	 * 
	 * @return firstElement of list.
	 */
	@Override
	public E first() {
		return header.getNext().getElement();
	}

	/**
	 * If list is not empty, returns the last element.
	 * 
	 * @return lastElement of list.
	 */
	@Override
	public E last() {
		return header.getPrev().getElement();
	}

	/**
	 * Gets object on index from list. If the index isn't valid, returns null.
	 * 
	 * @param index - index in the list for the object to be returned.
	 * 
	 * @return temp.getElement() - element at index from the temporary node.
	 */
	@Override
	public E get(int index) {
		if(index < 0 || index >= this.size() || this.isEmpty()) {
			return null;
		}
		Node<E> temp = header.getNext();
		for(int i = 0; i < index; i++) {
			temp = temp.getNext();
		}
		return temp.getElement();
	}

	/**
	 * Clears all objects from list.
	 */
	@Override
	public void clear() {
		while(!this.isEmpty()) {
			this.remove(0);
		}
	}

	/**
	 * Check if list contains the object.
	 * 
	 * @param e - object to searched for in list.
	 * 
	 * @return boolean - true if the object is found, false otherwise.
	 */
	@Override
	public boolean contains(E e) {
		if(this.isEmpty()) {
			return false;
		}
		Node<E> temp = header.getNext();
		while(temp != header) {
			if(temp.getElement().equals(e)) {
				return true;
			}
			temp = temp.getNext();
		}
		return false;
	}

	/**
	 * Returns if list is empty.
	 * 
	 * @return boolean - true if the list is empty, false if it is not.
	 */
	@Override
	public boolean isEmpty() {
		return this.size == 0;
	}


	/**
	 * Method that counts how many times it iterates before it finds the object, thus returning its index taking header.getNext() as position 0;
	 * 
	 * @return count - number of indexes that were passed, before finding the element, -1 if not found or list is empty.
	 */
	@Override
	public int firstIndex(E e) {
		if(this.isEmpty()) {
			return -1;
		}
		int count = 0;
		Node<E> temp = header.getNext();
		while(temp != header) {
			if(temp.getElement().equals(e)) {
				return count;
			}
			temp = temp.getNext();
			count++;
		}
		return -1;
	}

	/**
	 * Method that counts how many times it iterates before it finds the object, thus returning its index taking header.getPrev() as position size-1;
	 * 
	 * @return count - number of indexes that were passed, before finding the element, -1 if not found or list is empty.
	 */
	@Override
	public int lastIndex(E e) {
		if(this.isEmpty()) {
			return -1;
		}


		int count = this.size - 1;
		Node<E> temp = header.getPrev();
		while(temp != header) {
			if(temp.getElement().equals(e)) {
				return count;
			}
			temp = temp.getPrev();
			count--;
		}
		return -1;
	}

	/**
	 * Class to represent a node used in a doubly linked list
	 *
	 * Some methods were not used, but will leave in the final project in case they are needed in the future.
	 *
	 * @param <E>
	 */
	private static class Node<E> {
		private E element;
		private Node<E> next;
		private Node<E> previous;

		public Node(E element, Node<E> next, Node<E> prev) {
			super();
			this.element = element;
			this.next = next;
			this.previous = prev;
		}

		public Node(E element) {
			super();
			this.element = element;
			this.next = null;
			this.previous = null;
		}

		public Node() {
			super();
			this.element = null;
			this.next = null;
			this.previous = null;
		}

		public E getElement() {
			return element;
		}
		public void setElement(E element) {
			this.element = element;
		}
		public Node<E> getNext() {
			return next;
		}
		public void setNext(Node<E> next) {
			this.next = next;
		}

		public Node<E> getPrev() {
			return previous;
		}
		public void setPrev(Node<E> prev) {
			this.previous = prev;
		}

		/**
		 * Just set references prev and next to null. Disconnects the node
		 * from the linked list...
		 */
		public void cleanLinks() {
			this.next = null;
			this.previous = null;
			this.element = null;
		}
	}

}
