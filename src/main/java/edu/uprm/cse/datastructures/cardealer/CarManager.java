package edu.uprm.cse.datastructures.cardealer;

import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import edu.uprm.cse.datastructures.cardealer.model.Car;
import edu.uprm.cse.datastructures.cardealer.model.CarComparator;
import edu.uprm.cse.datastructures.cardealer.model.CarList;
import edu.uprm.cse.datastructures.cardealer.util.CircularSortedDoublyLinkedList;

/**
 * CarManager class that creates a list of Cars using CircularSortedDoublyLinkedList and manages REST operations based on a car dealer.
 * 
 * @author Carolina Santiago
 *
 */
@Path("/cars")
public class CarManager {
	private static CircularSortedDoublyLinkedList<Car> cList = CarList.getInstance(); 

	/**
	 * Gets all cars in the list.
	 * 
	 * @return array - array containing all cars in the list.
	 */
	@GET
	@Produces(MediaType.APPLICATION_JSON)	
	public Car[] getAllCars() {
		Car[] cars = new Car[cList.size()];
		for(int i =0; i < cList.size(); i++) {
			cars[i] = cList.get(i);
		}
		return cars;
	}

	/**
	 * Gets the car in the id given.
	 * 
	 * @param id - id of car.
	 * 
	 * @return car - if found returns car, else returns NotFoundException.
	 */
	@GET
	@Path("{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Car getCar(@PathParam("id") long id) throws NotFoundException{
		if (cList.isEmpty()) {
			throw new NotFoundException(new JsonError("Error", "Car " + id + " not found because list is empty"));
		}
		for(Car carIterator: cList) {
			if(id == carIterator.getCarId()) {
				return carIterator;
			}
		}
		throw new NotFoundException(new JsonError("Error", "Car " + id + " not found"));
	}  

	/**
	 * Adds a car to the list.
	 * 
	 * @param car - car to be added.
	 * 
	 * @return response to signal that the car was added to the list.
	 */
	@POST
	@Path("/add")
	@Produces(MediaType.APPLICATION_JSON)
	public Response addCar(Car car){
		cList.add(car); 
		return Response.status(Status.CREATED).build();
	}

	/**
	 * Updates the car with a given id.
	 * 
	 * @param car - car with the updated information.
	 * 
	 * @return if the car was updated successfully or if it was not found.
	 */
	@PUT
	@Path("{id}/update")
	@Produces(MediaType.APPLICATION_JSON)
	public Response updateCar(Car car){
		for(Car carIterator: cList) {
			if(car.getCarId() == carIterator.getCarId()) {
				cList.remove(carIterator);
				cList.add(car);
				return Response.status(Status.OK).build();
			}
		}
		return Response.status(Status.NOT_FOUND).build();  
	}

	/**
	 * Deletes the car of given id.
	 * 
	 * @param id - id of car to be deleted.
	 * 
	 * @return if the car was deleted successfully or if it was not found.
	 */

	@DELETE
	@Path("/{id}/delete")
	@Produces(MediaType.APPLICATION_JSON)
	public Response deleteCar(@PathParam("id") long id) {
		for(int i = 0; i < cList.size(); i++) {
			if(id == cList.get(i).getCarId()) {
				cList.remove(i);
				return Response.status(Status.OK).build();
			}
		}
		throw new NotFoundException(new JsonError("Error", "Car " + id + " not found"));
	}                               
}
